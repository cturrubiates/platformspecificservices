﻿using System;
using Unity;

namespace NetStandardCoreProject.Contracts {

    public interface IPlatformService {

        void Load(IUnityContainer Container);
    }



}
