﻿using System;
using NetStandardCoreProject.Utils;

namespace NetStandardCoreProject.ViewModels {

    public class SLMainViewModel : MainViewModel {

        public SLMainViewModel()
        {
        }

        public void Initialize()
        {
            DeviceInfo = CoreServiceLocator.DeviceInfo;
        }
    }
}
