﻿using System;
using System.Threading.Tasks;
using CrossDevicePlugin;
using CrossDevicePlugin.Contracts;
using NetStandardCoreProject.Utils;
using NetStandardCoreProject.Utils.IOC;
using Unity;
using Unity.Attributes;

namespace NetStandardCoreProject.ViewModels {

    public class IOCMainViewModel : MainViewModel {

        [Dependency]
        public override IDeviceInfo DeviceInfo {
            get; set;
        }

        public IOCMainViewModel() {
        }


    }
}
