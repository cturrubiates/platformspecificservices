﻿using System;
using CrossDevicePlugin.Contracts;

namespace NetStandardCoreProject.ViewModels {

    public abstract class MainViewModel {

        public virtual IDeviceInfo DeviceInfo { get; set; }
    }
}
