﻿using System;
using CrossDevicePlugin;
using CrossDevicePlugin.Contracts;

namespace NetStandardCoreProject.ViewModels {

    public class MTMainViewModel : MainViewModel {

        public MTMainViewModel()
        {
        }

        public void Initialize()
        {
            DeviceInfo = CrossDevice.DeviceInfo;
        }


    }
}
