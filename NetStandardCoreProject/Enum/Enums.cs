﻿using System;
namespace NetStandardCoreProject.Enum {

    public enum ViewModelType {
        ServiceLocator,
        IOC,
        MultiTarget
    }
}
