﻿using System;

namespace NetStandardCoreProject {

	[AttributeUsage (AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
	public class PlatformImplementationAttribute : Attribute
	{
		public Type Implementor {
			get;
			private set;
		}

		public PlatformImplementationAttribute (Type implementorType)
		{
			this.Implementor = implementorType;
		}
	}
}

