﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NetStandardCoreProject {
	public enum DependencyFetchTarget
	{
		GlobalInstance,
		NewInstance
	}

	public static class PlatformImplementationAllocator
	{
		//
		// Static Fields
		//
		private static bool initialized = false;

		private static readonly List<Type> DependencyTypes = new List<Type>();

        private static readonly Dictionary<Type, DependencyData> Implementations = new Dictionary<Type, DependencyData> ();

		//
		// Static Methods
		//
		private static IEnumerable<Type> FindImplementors (Type target)
		{
			return DependencyTypes.Where (target.IsAssignableFrom);
		}

		public static T Get<T> (DependencyFetchTarget fetchTarget = DependencyFetchTarget.GlobalInstance) where T : class
		{
			if (!initialized) 
			{
				Initialize ();
			}

			Type typeFromHandle = typeof(T);

			if (!Implementations.ContainsKey (typeFromHandle)) 
			{
				var types = FindImplementors (typeFromHandle);

				Implementations [typeFromHandle] = ((types != null && types.Any()) 
					? new DependencyData { ImplementorType = types.ToList() } 
					: null);
			} 
				
			DependencyData dependencyData = Implementations [typeFromHandle];
			if (dependencyData == null) {
				return default(T);
			}

			if (fetchTarget == DependencyFetchTarget.GlobalInstance) 
			{
				if (!dependencyData.GlobalInstance.Any()) 
				{
					foreach (var singleImplementorType in dependencyData.ImplementorType) 
					{
						var result = Activator.CreateInstance (singleImplementorType);
						dependencyData.GlobalInstance.Add(result);
					}
				}
				return (T)((object)dependencyData.GlobalInstance.FirstOrDefault());
			}
			return (T)((object)Activator.CreateInstance (dependencyData.ImplementorType.FirstOrDefault()));
		}

		#region Custom dependency array logic

		public static T[] GetAll<T> (DependencyFetchTarget fetchTarget = DependencyFetchTarget.GlobalInstance) where T : class
		{
			return GetAllInternal<T> (fetchTarget).ToArray();
		}

		private static IEnumerable<T> GetAllInternal<T> (DependencyFetchTarget fetchTarget = DependencyFetchTarget.GlobalInstance) where T : class
		{
			if (!initialized) 
			{
				Initialize ();
			}

			Type typeFromHandle = typeof(T);

			if (!Implementations.ContainsKey (typeFromHandle)) 
			{
				var types = FindImplementors(typeFromHandle);

				Implementations [typeFromHandle] = ((types != null && types.Any()) 
					? new DependencyData { ImplementorType = types.ToList() } 
					: null);
			} 

			DependencyData dependencyData = Implementations [typeFromHandle];
			if (dependencyData == null) 
			{
				yield return default(T);
			} 
			else 
			{
				if (fetchTarget == DependencyFetchTarget.GlobalInstance) 
				{
					if (!dependencyData.GlobalInstance.Any()) 
					{
						foreach (var singleImplementorType in dependencyData.ImplementorType) 
						{
							var result = Activator.CreateInstance (singleImplementorType);
							dependencyData.GlobalInstance.Add (result);
						}
					}
					foreach (var singleImplementor in dependencyData.GlobalInstance) 
					{
						yield return (T)((object)singleImplementor);
					}
				} 
				else 
				{
					foreach (var singleImplementorType in dependencyData.ImplementorType) 
					{
						yield return (T)((object)Activator.CreateInstance (singleImplementorType));
					}
				}
			}
		}

		#endregion

		private static void Initialize ()
		{
			Assembly[] assemblies = GetAssemblies ();
			Type typeFromHandle = typeof(PlatformImplementationAttribute);
			Assembly[] array = assemblies;

			for (int i = 0; i < array.Length; i++) 
			{
				Assembly element = array [i];
				Attribute[] array2 = element.GetCustomAttributes (typeFromHandle).ToArray<Attribute> ();
				if (array2.Length != 0) 
				{
					Attribute[] array3 = array2;
					for (int j = 0; j < array3.Length; j++) 
					{
						PlatformImplementationAttribute dependencyAttribute = (PlatformImplementationAttribute)array3 [j];

						if (!DependencyTypes.Contains (dependencyAttribute.Implementor)) 
						{
							DependencyTypes.Add (dependencyAttribute.Implementor);
						}
					}
				}
			}
			initialized = true;
		}

		public static Assembly[] GetAssemblies ()
        {
			var a = AppDomain.CurrentDomain.GetAssemblies();
			return a;
		}

		private class DependencyData
		{
			public DependencyData()
			{
				ImplementorType = new List<Type>();
				GlobalInstance = new List<object>();
			}

			public List<Type> ImplementorType 
			{
				get;
				set;
			}

			public List<object> GlobalInstance 
			{
				get;
				set;
			}
		}
	}
}
