﻿using System;
using CrossDevicePlugin.Contracts;


namespace NetStandardCoreProject.Utils {

	public static class CoreServiceLocator {

		public static IDeviceInfo DeviceInfo  {

			get{
				return PlatformImplementationAllocator.Get<IDeviceInfo>();
			}
            
		}


    }
}
