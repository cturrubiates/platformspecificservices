﻿using System;
using System.Collections.Generic;
using System.Linq;
using NetStandardCoreProject.Enum;
using NetStandardCoreProject.Utils.IOC;
using NetStandardCoreProject.ViewModels;
using Unity;

namespace NetStandardCoreProject.Utils {

    public class DeviceInfoStrategyFactory<T> where T:class
    {

        Dictionary<ViewModelType, Func<T>> strategies = new Dictionary<ViewModelType, Func<T>>() {
            { ViewModelType.IOC, CreateViewModelWithIOC }, 
                { ViewModelType.MultiTarget, CreateViewModelWithMuitiTargetPlugin },
                { ViewModelType.ServiceLocator, CreateViewModelWithServiceLocatorFunctionality }
                
            };

        private static T CreateViewModelWithServiceLocatorFunctionality()
        {
            SLMainViewModel vm = new SLMainViewModel();
            vm.Initialize();

            return vm as T  ;
        }

        private static T CreateViewModelWithMuitiTargetPlugin()
        {
            MTMainViewModel vm = new MTMainViewModel();
            vm.Initialize();

            return vm as T;
        }

        private static T CreateViewModelWithIOC()
        {
            IOCMainViewModel vm = IOCContainer.Container.Resolve<IOCMainViewModel>();
            return vm as T;
        }

        public T GetViewModel(ViewModelType viewModelType) {

            Func<T> func = strategies.FirstOrDefault(x => x.Key == viewModelType).Value;
            T vm = func();
            return vm;

        }



    }
}
