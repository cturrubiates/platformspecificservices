﻿using System;
using NetStandardCoreProject.Contracts;
using NetStandardCoreProject.ViewModels;
using Unity;

namespace NetStandardCoreProject.Utils.IOC {

    public class IOCContainer {

        public static IUnityContainer Container { get; private set; }

        public static void Setup(IPlatformService[] platformServices)
        {
            Container = new UnityContainer();

            RegisterPlatformSpecificServices(platformServices);
            RegisterViewModels();
        }

        static void RegisterModels()
        {

        }

        static void RegisterRepositories()
        {
            //repositories
            //container.RegisterType<UserRepository>();
        }

        static void RegisterViewModels()
        {
            Container.RegisterType<IOCMainViewModel>();
        }

        static void RegisterServices()
        {

        }

        static void RegisterPlatformSpecificServices(IPlatformService[] platformModules)
        {
            foreach (var platformSpecificModule in platformModules) {

                platformSpecificModule.Load(Container);
            }

        }
    }
}
