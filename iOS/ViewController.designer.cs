﻿// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace TestingNetStandardMultiPlatform.iOS
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        UIKit.UILabel deviceidlbl { get; set; }

        [Outlet]
        UIKit.UILabel issimulatorlbl { get; set; }

        [Outlet]
        UIKit.UILabel istabletlbl { get; set; }

        [Outlet]
        UIKit.UILabel manufacturerlbl { get; set; }

        [Outlet]
        UIKit.UILabel modellbl { get; set; }

        [Outlet]
        UIKit.UILabel namelbl { get; set; }

        [Outlet]
        UIKit.UILabel oslbl { get; set; }

        [Outlet]
        UIKit.UILabel osversionlbl { get; set; }

        [Outlet]
        UIKit.UILabel screenheightlbl { get; set; }

        [Outlet]
        UIKit.UILabel screenwidthlbl { get; set; }
        
        void ReleaseDesignerOutlets ()
        {
            if (deviceidlbl != null) {
                deviceidlbl.Dispose ();
                deviceidlbl = null;
            }

            if (namelbl != null) {
                namelbl.Dispose ();
                namelbl = null;
            }

            if (modellbl != null) {
                modellbl.Dispose ();
                modellbl = null;
            }

            if (manufacturerlbl != null) {
                manufacturerlbl.Dispose ();
                manufacturerlbl = null;
            }

            if (oslbl != null) {
                oslbl.Dispose ();
                oslbl = null;
            }

            if (osversionlbl != null) {
                osversionlbl.Dispose ();
                osversionlbl = null;
            }

            if (screenwidthlbl != null) {
                screenwidthlbl.Dispose ();
                screenwidthlbl = null;
            }

            if (screenheightlbl != null) {
                screenheightlbl.Dispose ();
                screenheightlbl = null;
            }

            if (issimulatorlbl != null) {
                issimulatorlbl.Dispose ();
                issimulatorlbl = null;
            }

            if (istabletlbl != null) {
                istabletlbl.Dispose ();
                istabletlbl = null;
            }
        }
    }
}
