﻿using Foundation;
using ObjCRuntime;
using UIKit;

namespace XamarinEssentialsExtensions
{
    public partial class DeviceInfo
    {
		string GetUUID() => UIDevice.CurrentDevice.IdentifierForVendor.ToString();

        string GetModel() => UIDevice.CurrentDevice.Model;

        string GetManufacturer() => "Apple";

        string GetDeviceName() => UIDevice.CurrentDevice.Name;

        string GetVersionString() => UIDevice.CurrentDevice.SystemVersion;

        //static string GetPlatform() => Platforms.iOS;

        //static string GetIdiom()
        //{
        //    switch (UIDevice.CurrentDevice.UserInterfaceIdiom)
        //    {
        //        case UIUserInterfaceIdiom.Pad:
        //            return Idioms.Tablet;
        //        case UIUserInterfaceIdiom.Phone:
        //            return Idioms.Phone;
        //        case UIUserInterfaceIdiom.TV:
        //            return Idioms.TV;
        //        case UIUserInterfaceIdiom.CarPlay:
        //        case UIUserInterfaceIdiom.Unspecified:
        //        default:
        //            return Idioms.Unsupported;
        //    }
        //}

        //static DeviceType GetDeviceType()
            //=> Runtime.Arch == Arch.DEVICE ? DeviceType.Physical : DeviceType.Virtual;


    }
}
