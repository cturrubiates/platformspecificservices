﻿//#define USE_SERVICELOCATOR
//#define USE_MULTITARGET
#define USE_IOC

using System;
using NetStandardCoreProject;
using NetStandardCoreProject.Contracts;
using NetStandardCoreProject.Enum;
using NetStandardCoreProject.Utils;
using NetStandardCoreProject.Utils.IOC;
using NetStandardCoreProject.ViewModels;
using TestingNetStandardMultiPlatform.iOS.PlatformServices;
using UIKit;
using Unity;

namespace TestingNetStandardMultiPlatform.iOS {

    public partial class ViewController : UIViewController {

        private MainViewModel vm;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InitProperties();
        }

        private void InitProperties()
        {
            DeviceInfoStrategyFactory<MainViewModel> vmfactory = new DeviceInfoStrategyFactory<MainViewModel>();
           
            #if USE_SERVICELOCATOR
                vm = vmfactory.GetViewModel(ViewModelType.ServiceLocator);
            #elif USE_MULTITARGET
                vm = vmfactory.GetViewModel(ViewModelType.MultiTarget);
            #elif USE_IOC
                //Normally this setup is made on App FinishedLaunching so this is just for learning purposes
                IOCContainer.Setup(new IPlatformService[] { new PlatformServiceModuleIOS() });

                vm = vmfactory.GetViewModel(ViewModelType.IOC);
            #endif
        
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

        }
        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();

            deviceidlbl.Text = vm.DeviceInfo.DeviceId;
            namelbl.Text = vm.DeviceInfo.DeviceName;
            modellbl.Text = vm.DeviceInfo.Model;
            manufacturerlbl.Text = vm.DeviceInfo.Manufacturer;
            oslbl.Text = vm.DeviceInfo.OperatingSystem;
            osversionlbl.Text = vm.DeviceInfo.OperatingSystemVersion;
            screenwidthlbl.Text = vm.DeviceInfo.ScreenWidth.ToString();
            screenheightlbl.Text = vm.DeviceInfo.ScreenHeight.ToString();
            istabletlbl.Text = (vm.DeviceInfo.IsTablet) ? "Running on IPad " : "Running on Iphone ";
            issimulatorlbl.Text = (vm.DeviceInfo.IsSimulator) ? "Simulator" : "Device";



        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.		
        }
    }
}
