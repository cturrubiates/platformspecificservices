﻿using System;
using CrossDevicePlugin.Contracts;
using NetStandardCoreProject.Contracts;
using Unity;

namespace TestingNetStandardMultiPlatform.iOS.PlatformServices {

    public class PlatformServiceModuleIOS : IPlatformService {

        public PlatformServiceModuleIOS()
        {
        }

        public void Load(IUnityContainer Container)
        {
            Container.RegisterType<IDeviceInfo, DeviceInfoImplementationIOS>();
        }
    }
}
