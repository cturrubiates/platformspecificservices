﻿using Android.App;
using Android.Widget;
using Android.OS;
using Acr.UserDialogs;
using NetStandardCoreProject;

namespace TestingNetStandardMultiPlatform.Droid {
    [Activity(Label = "TestingNetStandardMultiPlatform", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity {
        int count = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            UserDialogs.Init(this);
            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.myButton);

            button.Click += delegate { button.Text = $"{count++} clicks!"; };


        }



    }
}

