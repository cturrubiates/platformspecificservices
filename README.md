This project is intended for learning purposes.
You are gonna be able to see 3 different basic implementations for getting platform specific services , 
you are able to set the projects to work with any implementation

<b>Custom Service Locator </b> 

    Using the service locator is one way to achieve this kind of features , it is internally using reflection to load assemblies for any implementation that you specify with the PlatformImplementationAttribute , you have to enable the USE_SERVICELOCATOR directive


<b>Net standard 2 Multi target project</b>
    
using Net Standard project  and its new multi target functionality you can load platform specific features , you have to enable the USE_MULTITARGET directive


<b>IOCContainer</b> 
    IOC Container is another way , with this you have also the opportunity to inject dependencies trought constructor or property , to use it
 you have to enable the USE_IOC directive 
 
     At the moment of this writing there is just IOS implementation , maybe you can help me working with android implementation
    
        Author: CesTR
    



 

