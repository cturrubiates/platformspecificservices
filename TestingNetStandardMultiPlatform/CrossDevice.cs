﻿using System;
using CrossDevicePlugin.Contracts ;

namespace CrossDevicePlugin {

    public static partial class CrossDevice {

        static IDeviceInfo device;
        public static IDeviceInfo DeviceInfo {
            get {
                if (device == null)
                    throw new Exception("Device null");

                return device;
            }
            set => device = value;
        }

       
    }
}
