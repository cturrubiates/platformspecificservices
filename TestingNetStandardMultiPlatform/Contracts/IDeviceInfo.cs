﻿using System;
namespace CrossDevicePlugin.Contracts {

    public interface IDeviceInfo {

        int ScreenHeight { get; }
        int ScreenWidth { get; }

        string DeviceName { get; }
        string DeviceId { get; }
        string Model { get; }
        string Manufacturer { get; }
        string OperatingSystem { get; }
        string OperatingSystemVersion { get; }

        bool IsSimulator { get; }
        bool IsTablet { get; }

    }
}
